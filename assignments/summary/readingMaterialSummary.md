


### **What is IOT ?**

IOT means _Internet of Things_. The things/devices connected to the internet and transmits data from one place to another over the **internet**.

For Example :  Smart home security system.


![IOT](https://cdn.educba.com/academy/wp-content/uploads/2019/07/What-is-IOT.png)


---



### **Industry Revolution**


![Industry Revolution](https://cdn.britannica.com/44/197444-050-6AAA3995/Graph-progression-21st-Industrial-Revolutions.jpg)


---




### **Industry 3.0**


`Data is stored in Databases`

![Industry 3.0](https://gitlab.com/Utsav27/iotmodule1/-/raw/master/assignments/summary/images/2020-07-28__2_.png)





**Field Devices**

In above Industry 3.0 pyramid, the bottom side is _Field Devices_. These devices are either sensors or actuators connected inside the factory to measure final values.


**Control Device**

_Control Devices_  includes  either PCs,PLCs or DCS.



**Station**

Set of _Field Devices_ and _Control Devices_ is called one _Station_.


**Work Centers**

Set of stations are called _Work Centers_, like one factory.

**Enterprise**

Set of Work Center/factory are called an _Enterprise_.


---


### **Industry 3.0 Architecture**


![Industry 3.0 Architecture](https://gitlab.com/Utsav27/iotmodule1/-/raw/master/assignments/summary/images/2.png)


**Sensors and Actuators**

These are field devices communicate with controller using `Field Bus`.

**SCADA and ERP**

SCADA and ERP both are softwares. These stores and arrange the data. SCADA is on the work center level and ERP is on the enterprise level.


---


### **Industry 3.0 communication protocols**

1. MODBUS
2. PROFINET
3. CANopen
4. ETHERCAT
   

   ---


   ### **Industry 4.0 is Industry 3.0 devices connected to the internet (IOT)**

   Devices send data when they are connected over the internet.
   Then what can we do when data gathered from Devices......



   ![](https://gitlab.com/Utsav27/iotmodule1/-/raw/master/assignments/summary/images/3.png)


   ---


   ### **Industry 4.0 Architecture**


   ![Industry 4.0 Architecture](https://gitlab.com/Utsav27/iotmodule1/-/raw/master/assignments/summary/images/4.png)


   **EDGE Block**

   Edge devices or IOT Gateway get the data from controller and converts it into the protocol that be the cloud understand.


   ---


   ### **Industry 4.0 Communication protocols**

   1. MQTT
   2. AMQP
   3. OPC UA
   4. COAP
   5. Websockets
   6. HTTP

---


### **Problems with Industry 4.0 Upgrades**


1. Problem One : Cost
2. Problem Two : Downtime
3. Problem Three : Reliablity


### **Solutions**

- Design cheap devices to get the data from Industry 3.0 devices without disturbing it's original configurations and send data to cloud.
- To get the data from Industry 3.0 devices, convert Industry 3.0 protocols to Industry 4.0 protocols.


**Challenges in conversion**

1. Expensive Hardware
2. Lack of Documentation
3. Proprietary PLC protocols


---


### **Data send to the Internet....What next ?**

when our data is online, we need to store it. The data can be stores inside the _TSDB_.

`IOT TSDB tools`

TSDB is _Time Series Data Bases_. After sending data to cloud, the data get stored into TDBS.

Examples :

1. Prometheus
2. InfluxDB


`IOT Dashboards`

Visualization and analytics softwares 

Examples :

1. Grafana
2. Thingsboard


---


### **IOT Platforms**

1. AWS IOT
2. Google IOT
3. Azure IOT
4. Thingsboard


### **Get Alerts**

Get Alerts for devices and data via mails, whatsapp messages, SMS.

Example :

1. Zaiper
2. Twilio


---



